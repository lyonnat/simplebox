<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>网盘</title>
</head>
<body>
<?php
  date_default_timezone_set('Asia/Shanghai');

  /* 转换大小显示方式 */
  function adjsize($size = 0) {
    $size = $size > 1048576 ? sprintf('%.1lfM', $size/1048576) : sprintf('%.1lfK', $size/1024);
    return $size;
  }

  /* 获取并排序目录下的文件 */
  function lsdir($dir, $sort = 'name') {
    if ($dir == null) {
      return false;
    }
    $i = -1;
    $files = array('name'=>array(), 'time'=>array(), 'size'=>array(), 'dir'=>array(), 'print'=>array());
    $handle = opendir($dir);
    while($file = readdir($handle)) {
      if($file != '.' && $file != '..' && $file != 'index.php' && $file != '.htaccess') {
        $files['name'][++$i] = $file;
        $files['time'][++$i] = filemtime($file);
        $files['size'][++$i] = filesize($file);
        $files['dir'][++$i] = is_dir($file) ? true : false;
      }
    }
    closedir($handle);

    switch($sort) {
      case 'name': array_multisort($files['dir'],SORT_DESC, $files['name'], $files['time'],SORT_DESC, $files['size']);break;
      case 'time': array_multisort($files['dir'],SORT_DESC, $files['time'],SORT_DESC, $files['name'], $files['size']);;break;
      case 'size': array_multisort($files['dir'],SORT_DESC, $files['size'],SORT_DESC, $files['name'], $files['time'],SORT_DESC);break;
      default: array_multisort($files['dir'],SORT_DESC, $files['name'], $files['time'],SORT_DESC, $files['size']);
    }
    $files['count'] = count($files['name']);
    for ($i=0; $i<$files['count']; $i++) {
      $files['print'][$i] = $files['dir'][$i] ? $files['name'][$i].'/' : $files['name'][$i];
      $files['size'][$i] = $files['dir'][$i] ? '---' : adjsize($files['size'][$i]);
      $files['time'][$i] = $files['name'][$i] == '..' ? '' : date('Y-m-d H:i', $files['time'][$i]);
    }
	if(substr_count($_SERVER['PHP_SELF'], '/') > 1) {
      $files['count']++;
      $files['print'] = array_merge(array('上级目录/'), $files['print']);
      $files['name'] = array_merge(array('..'), $files['name']);
      $files['time'] = array_merge(array(''), $files['time']);
      $files['size'] = array_merge(array(''), $files['size']);
      $files['dir'] = array_merge(array(true), $files['dir']);
	}

    return $files;
  }
  $files = lsdir(getcwd(), 'time');

  /* 获取上传大小限制 */
  function bytesize($size = 0) {
    if (!$size) {
      return 0;
    }
    $scan['gb'] = 1073741824;
    $scan['g']  = 1073741824;
    $scan['mb'] = 1048576;
    $scan['m']  = 1048576;
    $scan['kb'] =    1024;
    $scan['k']  =    1024;
    $scan['b']  =       1;
    foreach ($scan as $unit => $factor) {
      if (strlen($size) > strlen($unit) && strtolower(substr($size, strlen($size) - strlen($unit))) == $unit) {
        return substr($size, 0, strlen($size) - strlen($unit)) * $factor;
      }
    }
    return $size;
  }
  $upload_max_filesize = bytesize(ini_get('upload_max_filesize'));
  $post_max_size = bytesize(ini_get('post_max_size'));
  $max_upload_size = $upload_max_filesize > $post_max_size ? $post_max_size : $upload_max_filesize;
  $max_file_size = $upload_max_filesize > $post_max_size ? ini_get('post_max_size') : ini_get('upload_max_filesize');

  /* 设置允许上传文件的类型 */
  $type = array('zip','rar','txt','jpg','png','doc','ppt','xls','docx','pptx','xlsx','pdf');
  $typetext = implode(',', $type);

  /* 上层目录 */
  $prntdir = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));

  /* 显示页脚 */
  function footer() { ?>
<a href="<?=$prntdir?>">返回网盘</a>
<div style="display:none"><script src="http://s9.cnzz.com/stat.php?id=2913133&web_id=2913133&show=pic1" language="JavaScript"></script></div>
</body>
</html>
  <?php }

  if ($_SERVER['REQUEST_METHOD'] == 'GET') { ?>
<table width="560">
<tr><th>名称</th><th align="right">文件大小</th><th align="right">上传日期 ↓</th></tr>
<tr><th colspan="3"><hr></th></tr>
<?php
  for ($i=0; $i<$files['count']; $i++) {
    echo '<tr><td><a href="'.$files['name'][$i].'">'.$files['print'][$i].'</a></td><td align="right">'.$files['size'][$i].'</td><td align="right">'.$files['time'][$i].'</td></tr>';
  }
?>
<tr><th colspan="3"><hr></th></tr>
</table>
<form action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" method="POST">
<input type="file" name="file"><br />
<input type="submit" name="submit" value="上传文件" />
</form>
<p>
大小限制：<?=$max_file_size?>B<br />
类型限制：<?=$typetext?><br />
</p>
<form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
<input type="text" name="dirname">
<input type="submit" name="submit" value="创建目录" />
</form>
<?php  }
  elseif (isset($_POST['dirname'])) {
    $dir = $_POST['dirname'];
    if (preg_match('/^[\w一-龥\-\(\)]+$/', $dir)) {
      if (file_exists($dir)) {
        echo '已存在同名文件或目录<br />';
      }
      else {
        mkdir($dir);
        copy(substr(strrchr($_SERVER['PHP_SELF'], '/'), 1), $dir.'/'.substr(strrchr($_SERVER['PHP_SELF'], '/'), 1));
        echo '目录 '.$dir.' 创建成功!<br />';
      }
    }
    else {
      if ($dir == '') {
        echo '目录名为空<br />';
      }
      else {
          echo '目录名 '.$dir.' 存在非法字符<br />';
      }
    }
    footer();
    exit();
  }
  else {
    /* 设定上传目录 */
    $dest_dir = '.';

    /* 检测上传目录是否存在 */
    if (!is_dir($dest_dir) || !is_writeable($dest_dir)) {
      echo '上传目录 '.$dest_dir.' 不存在或无法写入';
      footer();
      exit();
    }

    /* 获取上传文件信息 */
    if ($_FILES['file'] == NULL) {
      echo '文件过大或文件上传失败<br />';
      footer();
      exit();
    }
    else {
      $upfile = $_FILES['file'];
    }

    /* 获取文件后缀名函数 */
    function fileext($filename) {
      return substr(strrchr($filename, '.'), 1);
    }

    /* 判断上传文件类型 */
    if (!empty($upfile['name']) && !in_array(strtolower(fileext($upfile['name'])), $type)) {
      echo '对不起，您只能上传以下类型文件:<br />'.$typetext.'<br />';
    }
    else {
      /* 设置文件名为"日期_文件名" */
      $dest = $dest_dir.'/'.$upfile['name'];

      /* 移动上传文件到指定文件夹 */
      $state = move_uploaded_file($upfile['tmp_name'], $dest);

      if ($state) {
        print('文件上传成功!<br />');
        print('文件名：'.$upfile['name'].'<br />');
        print('上传的文件大小：'.(round($upfile['size']/1024,2)).' KB<br />');
      }
      else {
        /* 处理错误信息 */
        switch($upfile['error']) {
          case 1 : echo'上传文件大小超出 '.ini_get('upload_max_filesize').' 的限制<br />';break;
          case 2 : echo'上传文件大小超出 '.ini_get('post_max_size').' 的限制<br />';break;
          case 3 : echo'文件仅被部分上传<br />';break;
          case 4 : echo'没有文件被上传<br />';break;
          case 5 : echo'找不到临时文件夹<br />';break;
          case 6 : echo'文件写入失败<br />';break;
        }
      }
    }
    ?><a href="<?=$prntdir?>">返回网盘</a><?php
  }
?>
<div style="display:none"><script src="http://s9.cnzz.com/stat.php?id=2913133&web_id=2913133&show=pic1" language="JavaScript"></script></div>
</body>
</html>
